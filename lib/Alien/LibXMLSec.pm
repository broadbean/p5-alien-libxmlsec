package Alien::LibXMLSec;
use strict;
use warnings;
use parent 'Alien::Base';
our $VERSION = '0.007';

# on Jenkins, we don't have the static version of liblzma, so our
# programs wouldn't link; we don't seem to need it, so we remove it
sub libs_static {
    my $self = shift;
    my $flags = $self->SUPER::libs_static;
    $flags =~ s/-llzma\b//;
    return $flags;
}

1;
__END__

=head1 NAME

Alien::LibXMLSec - Build and install xmlsec1, an XML crypto library

=head1 SYNOPSIS

     use ExtUtils::MakeMaker;
     use Alien::Base::Wrapper qw( Alien::LibXMLSec !export );

     WriteMakefile(
       ...
       CONFIGURE_REQUIRES => {
         'Alien::Base::Wrapper' => '0',
         'Alien::LibXMLSec'     => '0',
       },
       Alien::Base::Wrapper->mm_args
       ...
     );

=head1 DESCRIPTION

This distribution provides an alien wrapper for C<xmlsec1>. It
requires a C compiler. That's all!

=head1 SEE ALSO

L<https://www.aleksey.com/xmlsec/index.html> the C<xmlsec> library

=head1 AUTHOR

Gianni Ceccarelli <gianni.ceccarelli@broadbean.com>

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Broadbean UK. C<xmlsec1> written and copyrighted by
Aleksey Sanin and others. Both C<xmlsec1> and this distribution are
released under the terms of the B<MIT License> and may be modified
and/or redistributed under the same or any compatible license.

=cut

#!perl
use Test::More;
use Test::Alien;
use Alien::LibXMLSec;

alien_ok 'Alien::LibXMLSec';

subtest compile => sub {
    xs_ok do { local $/; <DATA> }, with_subtest {
        is Foo::testInit(), 0, 'xmlSecInit() should return 0';
        is Foo::testVersion(), 1, 'xmlSecCheckVersion() should return 1';
    };
};

done_testing;

__DATA__
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include <xmlsec/xmlsec.h>

MODULE = Foo PACKAGE = Foo

int testInit()
  CODE:
    RETVAL = xmlSecInit();
  OUTPUT:
    RETVAL

int testVersion()
  CODE:
    RETVAL = xmlSecCheckVersion();
  OUTPUT:
    RETVAL
